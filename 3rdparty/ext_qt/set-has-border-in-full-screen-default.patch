diff --git a/src/platformheaders/windowsfunctions/qwindowswindowfunctions.h b/src/platformheaders/windowsfunctions/qwindowswindowfunctions.h
index e51c2fde6..032dcafa6 100644
--- a/src/platformheaders/windowsfunctions/qwindowswindowfunctions.h
+++ b/src/platformheaders/windowsfunctions/qwindowswindowfunctions.h
@@ -81,6 +81,15 @@ public:
             func(window, border);
     }
 
+    typedef void (*SetHasBorderInFullScreenDefault)(bool border);
+    static const QByteArray setHasBorderInFullScreenDefaultIdentifier() { return QByteArrayLiteral("WindowsSetHasBorderInFullScreenDefault"); }
+    static void setHasBorderInFullScreenDefault(bool border)
+    {
+        auto func = reinterpret_cast<SetHasBorderInFullScreenDefault>(QGuiApplication::platformFunction(setHasBorderInFullScreenDefaultIdentifier()));
+        if (func)
+            func(border);
+    }
+
     typedef void (*SetWindowActivationBehaviorType)(WindowActivationBehavior);
     static const QByteArray setWindowActivationBehaviorIdentifier() { return QByteArrayLiteral("WindowsSetWindowActivationBehavior"); }
 
diff --git a/src/platformheaders/windowsfunctions/qwindowswindowfunctions.qdoc b/src/platformheaders/windowsfunctions/qwindowswindowfunctions.qdoc
index a52bbe061..0c52cde75 100644
--- a/src/platformheaders/windowsfunctions/qwindowswindowfunctions.qdoc
+++ b/src/platformheaders/windowsfunctions/qwindowswindowfunctions.qdoc
@@ -93,7 +93,40 @@
     is true then it will enable the WS_BORDER flag in full screen mode to enable other top level windows
     inside the application to appear on top when required.
 
+    \note The setting must be applied before showing the window or switching it
+    to full screen. For QML, setHasBorderInFullScreenDefault() can be used to
+    set a default value.
+
+    See also \l [QtDoc] {Fullscreen OpenGL Based Windows}
+*/
+
+/*!
+    \typedef QWindowsWindowFunctions::SetHasBorderInFullScreenDefault
+    \since 5.13
+
+    This is the typedef for the function returned by QGuiApplication::platformFunction
+    when passed setHasBorderInFullScreenDefaultIdentifier.
+*/
+
+/*!
+    \fn QByteArray QWindowsWindowFunctions::setHasBorderInFullScreenDefaultIdentifier()
+    \since 5.13
+
+    This function returns the bytearray that can be used to query
+    QGuiApplication::platformFunction to retrieve the SetHasBorderInFullScreen function.
+*/
+
+/*!
+    \fn void QWindowsWindowFunctions::setHasBorderInFullScreenDefault(bool border)
+    \since 5.13
+
+    This is a convenience function that can be used directly instead of resolving
+    the function pointer. \a border will be relayed to the function retrieved by
+    QGuiApplication. When \a border is true, the WS_BORDER flag will be set
+    in full screen mode for all windows by default.
+
     See also \l [QtDoc] {Fullscreen OpenGL Based Windows}
+    \sa setHasBorderInFullScreen()
 */
 
 /*!
diff --git a/src/plugins/platforms/windows/qwindowsnativeinterface.cpp b/src/plugins/platforms/windows/qwindowsnativeinterface.cpp
index 1c5be4415..42193baf4 100644
--- a/src/plugins/platforms/windows/qwindowsnativeinterface.cpp
+++ b/src/plugins/platforms/windows/qwindowsnativeinterface.cpp
@@ -293,6 +293,8 @@ QFunctionPointer QWindowsNativeInterface::platformFunction(const QByteArray &fun
         return QFunctionPointer(QWindowsWindow::setTouchWindowTouchTypeStatic);
     if (function == QWindowsWindowFunctions::setHasBorderInFullScreenIdentifier())
         return QFunctionPointer(QWindowsWindow::setHasBorderInFullScreenStatic);
+    if (function == QWindowsWindowFunctions::setHasBorderInFullScreenDefaultIdentifier())
+        return QFunctionPointer(QWindowsWindow::setHasBorderInFullScreenDefault);
     if (function == QWindowsWindowFunctions::setWindowActivationBehaviorIdentifier())
         return QFunctionPointer(QWindowsNativeInterface::setWindowActivationBehavior);
     if (function == QWindowsWindowFunctions::isTabletModeIdentifier())
diff --git a/src/plugins/platforms/windows/qwindowswindow.cpp b/src/plugins/platforms/windows/qwindowswindow.cpp
index 76a443a89..967d479a0 100644
--- a/src/plugins/platforms/windows/qwindowswindow.cpp
+++ b/src/plugins/platforms/windows/qwindowswindow.cpp
@@ -1183,6 +1183,7 @@ QWindowCreationContext::QWindowCreationContext(const QWindow *w,
 
 const char *QWindowsWindow::embeddedNativeParentHandleProperty = "_q_embedded_native_parent_handle";
 const char *QWindowsWindow::hasBorderInFullScreenProperty = "_q_has_border_in_fullscreen";
+bool QWindowsWindow::m_borderInFullScreenDefault = false;
 
 QWindowsWindow::QWindowsWindow(QWindow *aWindow, const QWindowsWindowData &data) :
     QWindowsBaseWindow(aWindow),
@@ -1220,7 +1221,7 @@ QWindowsWindow::QWindowsWindow(QWindow *aWindow, const QWindowsWindowData &data)
 
     if (aWindow->isTopLevel())
         setWindowIcon(aWindow->icon());
-    if (aWindow->property(hasBorderInFullScreenProperty).toBool())
+    if (m_borderInFullScreenDefault || aWindow->property(hasBorderInFullScreenProperty).toBool())
         setFlag(HasBorderInFullScreen);
     clearFlag(WithinCreate);
 }
@@ -2823,6 +2824,11 @@ void QWindowsWindow::setHasBorderInFullScreenStatic(QWindow *window, bool border
         window->setProperty(hasBorderInFullScreenProperty, QVariant(border));
 }
 
+void QWindowsWindow::setHasBorderInFullScreenDefault(bool border)
+{
+    m_borderInFullScreenDefault = border;
+}
+
 void QWindowsWindow::setHasBorderInFullScreen(bool border)
 {
     if (testFlag(HasBorderInFullScreen) == border)
diff --git a/src/plugins/platforms/windows/qwindowswindow.h b/src/plugins/platforms/windows/qwindowswindow.h
index 2675990cf..0d8096ddf 100644
--- a/src/plugins/platforms/windows/qwindowswindow.h
+++ b/src/plugins/platforms/windows/qwindowswindow.h
@@ -342,6 +342,7 @@ public:
     static void setTouchWindowTouchTypeStatic(QWindow *window, QWindowsWindowFunctions::TouchWindowTouchTypes touchTypes);
     void registerTouchWindow(QWindowsWindowFunctions::TouchWindowTouchTypes touchTypes = QWindowsWindowFunctions::NormalTouch);
     static void setHasBorderInFullScreenStatic(QWindow *window, bool border);
+    static void setHasBorderInFullScreenDefault(bool border);
     void setHasBorderInFullScreen(bool border);
     static QString formatWindowTitle(const QString &title);
 
@@ -387,6 +388,7 @@ private:
     // note: intentionally not using void * in order to avoid breaking x86
     VkSurfaceKHR m_vkSurface = 0;
 #endif
+    static bool m_borderInFullScreenDefault;
 };
 
 #ifndef QT_NO_DEBUG_STREAM
